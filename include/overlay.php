<div class="adminLoader">
	<div class="adminLoader-spinner">
		<i class="adminLoader-icon dashicons dashicons-update"/></i>
	</div>
	<button class="adminLoader-message js-removeLoader">
		<?php _e('An error has occurred ? Click here to get back to the admin page.', 'krr-admin-loader') ?>
	</button>
</div>