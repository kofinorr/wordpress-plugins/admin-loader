<?php
/**
 * Plugin Name: Admin Loader
 * Description: Add a loader in your administration while your page is loading.
 * Version: 1.0.0
 * Author: Kalimorr
 * Author URI: https://gitlab.com/kalimorr/
 * Text Domain: krr-admin-loader
 * Domain Path: /languages
 * License: GPLv3 or later
 */

namespace KrrAdminLoader;

/**
 * Class AdminLoader
 * @package KrrAdminLoader
 */
class AdminLoader
{

	/**
	 * adminLoader constructor.
	 */
	public function __construct()
	{
		add_action('init', [$this, 'load_i18n']);
		add_action('admin_enqueue_scripts', [$this, 'enqueueScripts'], 11);
		add_filter('admin_body_class', [$this, 'bodyClass']);
		add_action('in_admin_footer', [$this, 'overlayRenderer']);
	}

	/**
	 * Load plugin textdomain
	 */
	public function load_i18n() {
		load_plugin_textdomain(
			'krr-admin-loader',
			false,
			dirname(plugin_basename(__FILE__)) . '/languages'
		);
	}

	/**
	 *  Styles and scripts
	 */
	public function enqueueScripts()
	{
		wp_enqueue_script('admin-loader', plugin_dir_url(__FILE__) . 'assets/script.js', [], false, true);
		wp_enqueue_style('admin-loader', plugin_dir_url(__FILE__) . 'assets/style.css', [], false);
	}

	/**
	 *  Add the loading class to the body
	 */
	public function bodyClass($classes)
	{
		return $classes . ' loadpage';
	}

	/**
	 *  Ajout de contenu dans le footer de l'administration
	 */
	public function overlayRenderer()
	{
		include 'include/overlay.php';
	}
}

new AdminLoader();