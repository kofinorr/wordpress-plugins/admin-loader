# Admin Loader
Add a loader in your administration while your page is loading.

## Requirements
* Require WordPress 4.7+ / Tested up to 5.6
* Require PHP 5.6

## Installation

### Manual
- Download and install the plugin using the built-in WordPress plugin installer.
- No settings necessary, it's a Plug-and-Play plugin !

### Composer
- Add the following repository source : 
```
{
    "type": "vcs",
    "url": "https://gitlab.com/kalimorr/wordpress-plugins/admin-loader.git"
}
```
- Include `"kalimorr/admin-loader": "dev-master"` in your composer file for last master's commits or a tag released.
- No settings necessary, it's a Plug-and-Play plugin !

## Internationalisation
* English *(default)*
* Français
* Spanish
* Italiano

## License
"Admin Loader" is licensed under the GPLv3 or later.