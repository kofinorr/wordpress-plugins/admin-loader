msgid ""
msgstr ""
"Project-Id-Version: Admin Loader\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-01 15:01+0000\n"
"PO-Revision-Date: 2020-05-01 15:02+0000\n"
"Last-Translator: \n"
"Language-Team: English (United States)\n"
"Language: en_US\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.3.3; wp-5.4.1"

#. Description of the plugin
msgid "Add a loader in your administration while your page is loading."
msgstr "Add a loader in your administration while your page is loading."

#. Name of the plugin
msgid "Admin Loader"
msgstr "Admin Loader"

#: include/overlay.php:6
msgid "An error has occurred ? Click here to get back to the admin page."
msgstr "An error has occurred ? Click here to get back to the admin page."

#. Author URI of the plugin
msgid "https://github.com/Kalimorr"
msgstr "https://github.com/Kalimorr"

#. Author of the plugin
msgid "Kalimorr"
msgstr "Kalimorr"
